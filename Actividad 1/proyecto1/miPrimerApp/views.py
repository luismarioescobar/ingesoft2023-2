from django.shortcuts import render,HttpResponse
from .models import *
from django.db.models import Count
from django.db.models import Count, Subquery, OuterRef

# Create your views here.


def index(request):
    duplicados =  Estudiante.objects.values('apellidos').annotate(Count('pk')).filter(pk__count__gt= 1)
    dup_order =  Estudiante.objects.order_by('apellidos').filter(apellidos__in= [es['apellidos'] for es in duplicados])

    edades = Estudiante.objects.values('edad').annotate(Count('edad')).filter(edad__count__gt= 1)
    edades_r = Estudiante.objects.filter(edad__in=Subquery(edades.values('edad'))).order_by('edad')

    edades_3 =Estudiante.objects.filter(grupo=3).values('edad').annotate(num_edades=Count('edad')).filter(num_edades__gt=1)
    same_3 = Estudiante.objects.filter(grupo=3, edad__in=Subquery(edades_3.values('edad')))
    
    contexto = {
        'grupo1': Estudiante.objects.filter(grupo=1),
        'grupo4': Estudiante.objects.filter(grupo=4),
        'mismo_apellido': dup_order,
        'misma_edad':edades_r,
        'grupo3_edad': same_3,  #Se trabaja en la consulta pensando a que se refiere a todos los alumnos del grupo 3 que tengan la  misma edad
        'all': Estudiante.objects.order_by('grupo').all()
    }
    return render(request, 'index.html', contexto)